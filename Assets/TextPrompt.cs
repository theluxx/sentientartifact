﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextPrompt : MonoBehaviour
{
    public static TextPrompt Instance;
    public TextMeshProUGUI textMesh;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void DisplayText(string text)
    {
        textMesh.text = text;
    }
}

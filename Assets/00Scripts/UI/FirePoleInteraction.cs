﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class FirePoleInteraction : Interaction, IInteractable
{
    [SerializeField] private PlayableDirector director;

    
    public void OnInteractionBegin()
    {
        if (GameObject.FindGameObjectWithTag("Player").transform.position.y < 5)
        {
            return;
        }
        director.Play();
    }

    public void OnInteractionExit()
    {
        //isDisplayed = false;
    }
}
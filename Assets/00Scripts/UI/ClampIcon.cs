﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClampIcon : MonoBehaviour
{

    [SerializeField] private RawImage icon;
    [SerializeField] private Vector3 offset;
    [SerializeField] private Transform lookAt;

    // Update is called once per frame
    void LateUpdate()
    {
        transform.LookAt(this.transform.position + Camera.main.transform.forward);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityHighlight : MonoBehaviour
{



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Object"))
        {
            if (other.gameObject.GetComponent<Interaction>())
            {
                //Debug.Log("Object is " + other.gameObject.name);
                other.gameObject.GetComponent<Interaction>().EnableHighlight(true);
            }

        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Object"))
        {
            if (other.gameObject.GetComponent<Interaction>())
            {
                Debug.Log("Object is " + other.gameObject.name);
                other.gameObject.GetComponent<Interaction>().EnableHighlight(false);
                
            }

        }
    }
}

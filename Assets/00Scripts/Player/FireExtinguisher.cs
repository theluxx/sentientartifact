﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireExtinguisher : MonoBehaviour
{
    [SerializeField] private GameObject extinguishParticles;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            extinguishParticles.SetActive(true);
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            extinguishParticles.SetActive(false);
        }
        
    }
}

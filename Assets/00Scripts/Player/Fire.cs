﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{

    [SerializeField] float hp = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Water"))
        {
            hp -= Time.deltaTime;
            float ratio = ((hp / 5f) * Time.deltaTime);
            transform.localScale -= (transform.localScale * ratio);
            if (hp < 1.5f)
            {
                this.gameObject.SetActive(false);
            }
        }
    }
}

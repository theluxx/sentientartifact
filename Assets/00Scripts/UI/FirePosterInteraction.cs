﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePosterInteraction : Interaction, IInteractable
{
    [SerializeField] private GameObject popupCanvas;
    private bool isDisplayed;

    
    public void OnInteractionBegin()
    {
        if (isDisplayed)
        {
            return;
        }
        GameObject poster = Instantiate(popupCanvas);
        poster.transform.SetParent(this.transform);
        isDisplayed = true;
    }

    public void OnInteractionExit()
    {
        isDisplayed = false;
    }
}

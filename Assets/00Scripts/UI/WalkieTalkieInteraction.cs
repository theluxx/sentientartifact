﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class WalkieTalkieInteraction : Interaction, IInteractable
{
    [SerializeField] private PlayableDirector director;
    public GameObject exitDoorTrigger;
    public GameObject exitDoorHighlight;
    
    public void OnInteractionBegin()
    {

        director.Play();
        this.gameObject.SetActive(false);
        exitDoorTrigger.GetComponent<BoxCollider>().enabled = true;
        exitDoorHighlight.SetActive(true);
    }

    public void OnInteractionExit()
    {
        //isDisplayed = false;
    }
}
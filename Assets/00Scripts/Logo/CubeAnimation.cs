﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CubeAnimation : MonoBehaviour
{

    [SerializeField] private Animator[] cubeAnimators;
    [SerializeField] private Animator[] cubeAnimators2;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(AnimationSequence());
        StartCoroutine(LoadNextScene());
    }


    public IEnumerator AnimationSequence()
    {
        float wait = .3f;
        for (int i = 0; i < cubeAnimators.Length; i++)
        {
            cubeAnimators[i].gameObject.SetActive(true);
            if (i == 2)
            {
                cubeAnimators2[0].gameObject.SetActive(true);
                wait = .2f;
            }
            if (i == 3)
            {
                cubeAnimators2[1].gameObject.SetActive(true);
                cubeAnimators2[2].gameObject.SetActive(true);
                wait = .1f;
            }
            yield return new WaitForSeconds(wait);
        }
    }

    public IEnumerator LoadNextScene()
    {
        yield return new WaitForSeconds(8);
        SceneManager.LoadScene(1);
    }
    
}

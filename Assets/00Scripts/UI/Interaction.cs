﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Interaction : MonoBehaviour
{
    [FormerlySerializedAs("anim")] public Animator actionHighlightAnimator;
    public Outline[] outline;
    public string textToDisplay = "Examine";
        
    public void EnableHighlight(bool enable)
    {
        if (enable)
        {
            actionHighlightAnimator.SetTrigger("FadeIn");
            for (int i = 0; i < outline.Length; i++)
            {
                outline[i].enabled = true;
            }
        }
        else
        {
            actionHighlightAnimator.SetTrigger("FadeOut");
            for (int i = 0; i < outline.Length; i++)
            {
                outline[i].enabled = false;
            }
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class RaycastInteract : MonoBehaviour
{
    private GameObject _castedObject;
    [SerializeField] private Animator anim;
    [SerializeField] private Interaction currentInteraction;
    [SerializeField] private bool faded;
    [SerializeField] private RawImage image;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float rayDistance = 5;

    // Update is called once per frame
    private void Update()
    {
        RaycastHit hit;
        var fwd = transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(transform.position, fwd, out hit, rayDistance, layerMask.value))
        {
            if (hit.collider.CompareTag("Object"))
            {
                if (faded == false)
                {
                    anim.SetTrigger("PromptFadeIn");
                    faded = true;
                }

                if (hit.collider.gameObject.GetComponent<Interaction>() != currentInteraction)
                {
                    TextPrompt.Instance.DisplayText(hit.collider.gameObject.GetComponent<Interaction>().textToDisplay);
                    currentInteraction = hit.collider.gameObject.GetComponent<Interaction>();
                }

                if (Input.GetMouseButtonDown(0))
                    if (hit.collider.gameObject.GetComponent<IInteractable>() != null)
                    {
                        hit.collider.gameObject.GetComponent<IInteractable>().OnInteractionBegin();
                    }
            }
        }
        else
        {
            if (faded)
            {
                anim.SetTrigger("PromptFadeOut");
                faded = false;
            }
        }
    }
}
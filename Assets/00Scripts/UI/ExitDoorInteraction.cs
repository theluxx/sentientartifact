﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.Playables;

public class ExitDoorInteraction : Interaction, IInteractable
{

    [SerializeField] private FirstPersonController playerController;
    [SerializeField] private PlayableDirector _director;
    
    public void OnInteractionBegin()
    {
        StartCoroutine(LoadNextScene());
    }

    public void OnInteractionExit()
    {
        
    }

    public IEnumerator LoadNextScene()
    {
        playerController.enabled = false;
        _director.Play();
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(2);
            
    }
}
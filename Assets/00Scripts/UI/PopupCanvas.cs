﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PopupCanvas : MonoBehaviour
{
    private FirstPersonController _controller;
    // Start is called before the first frame update
    void Awake()
    {
        _controller = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>();
        _controller.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Debug.Log("I should enable the cursor");
    }

    public void ClosePopup()
    {
        Cursor.visible = false;
        _controller.enabled = true;
        this.transform.parent.GetComponent<IInteractable>().OnInteractionExit();
        Destroy(this.gameObject);
        
    }
}
